import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import inputCode from './screens/inputCode'
import treatmentList from './screens/treatmentList'
import TreatmentDetail from './screens/treatmentDetail'

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen 
        name="Inicio" 
        component={inputCode} 
        options={{
          title:'',
          headerStyle:{
            height:0
          }
        }}/>
        <Stack.Screen
        name='ListaTratamientos'
        component={treatmentList}/>
        <Stack.Screen
        name='Detail'
        component={TreatmentDetail}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;