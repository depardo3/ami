import React, { useState } from 'react'
import { View, Text, FlatList, StyleSheet, Modal, TouchableOpacity } from 'react-native'
import { Calendar, CalendarList, Agenda, calendarTheme } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
import LabelTomaActividad from '../components/toma_actividad'
import NotificationMed from '../components/notificacionMed'
import ConfirmarTomaButton from '../assets/confirmarToma.svg'
import PosponerButton from '../assets/posponerToma.svg'
import CancelarActividad from '../assets/cancelarActividad.svg'
import ConfirmarActividad from '../assets/confirmarActividad.svg'

const DATATEST = [
    { key: '1', hora: '03:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '2', hora: '04:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '3', hora: '05:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '4', hora: '06:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '5', hora: '07:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '6', hora: '08:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '7', hora: '09:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '8', hora: '10:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '9', hora: '11:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '10', hora: '03:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '11', hora: '02:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '12', hora: '01:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '13', hora: '07:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
    { key: '14', hora: '08:00', tipo: 'Rehabilitacion', main: 'EJERCICIO' },
    { key: '15', hora: '14:00', tipo: 'Medicamento', main: 'IBUPROFENO' },
]
export default function TreatmentDetail({ navigation }) {

    const [visibleModalMed, setvisibleModalMed] = useState(false);
    const [visibleModalAct, setvisibleModalAct] = useState(false);
    const [dataModalAct, setdataModalAct] = useState({ hora: '1', tipo: '2', main: '3' });

    LocaleConfig.locales['ec'] = {
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene.', 'Feb.', 'Mar.', 'Abr.', 'May.', 'Jun.', 'Jul.', 'Ago.', 'Sep.', 'Oct.', 'Nov.', 'Dic.'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom..', 'Lun.', 'Mar.', 'Mier.', 'Jue.', 'Vier.', 'Sab.'],
        today: 'Hoy\'Hoy'
    };
    LocaleConfig.defaultLocale = 'ec';
    return (
        <View>
            {/*Modal Medicamento*/}
            <Modal
                visible={visibleModalMed}
                onRequestClose={() => { setvisibleModalMed(false); }}
                transparent={true}>
                <View style={Styles.modal}>
                    <View style={Styles.modalMedContainer}>
                        <NotificationMed>
                        </NotificationMed>
                        <TouchableOpacity><ConfirmarTomaButton width={250} height={100} style={{marginBottom:-30}}/></TouchableOpacity>
                        <TouchableOpacity><PosponerButton width={250} height={100}/></TouchableOpacity>
                    </View>
                </View>
            </Modal>

            {/*Modal Actividad*/}
            <Modal
                visible={visibleModalAct}
                onRequestClose={() => { setvisibleModalAct(false); }}
                transparent={true}>
                <View style={Styles.modal}>
                    <View style={{backgroundColor:'white', height:270, borderRadius:10, width:'90%', marginLeft:'auto', marginRight:'auto', padding:10}}>
                        <Text><Text style={{fontWeight:'bold'}}>Hola</Text> Andres </Text>
                        <Text>Quieres avisarle a tu doctor que vas hacer la actividad</Text>
                        <LabelTomaActividad hora={dataModalAct.hora} tipo={dataModalAct.tipo} main={dataModalAct.main}></LabelTomaActividad>
                        <View style={{flexDirection:'row', justifyContent:'center'}}>
                        <TouchableOpacity><CancelarActividad width={180} height={80} style={{marginRight:-25}}/></TouchableOpacity>
                        <TouchableOpacity><ConfirmarActividad width={180} height={80}/></TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>



            <CalendarList
                horizontal={true}
                pagingEnabled={false}
                calendarHeight={140}></CalendarList>
            <Text style={{ padding: 10 }}>Actividades Diarias</Text>
            <FlatList
                style={Styles.list}
                data={DATATEST}
                renderItem={({ item }) =>
                    <TouchableOpacity
                        onPress={() => {
                            if (item.tipo == 'Medicamento') {
                                setvisibleModalMed(true);
                            } else {
                                setvisibleModalAct(true);
                                setdataModalAct({ hora: item.hora, tipo: item.tipo, main: item.main });
                            }
                        }}>
                        <LabelTomaActividad hora={item.hora} tipo={item.tipo} main={item.main}></LabelTomaActividad></TouchableOpacity>}
            />
        </View>
    )
}

const Styles = StyleSheet.create({
    list: {
        marginBottom: 180
    },
    modalMedContainer: {
        marginTop: 'auto',
        marginBottom: 'auto',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '80%',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 20
    },
    btnConf: {
        borderRadius: 30,
        marginTop: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: '#32CCD6',
        height: 40,
        width: 200,
        padding: 5
    },
    btnCancel: {
        borderRadius: 30,
        marginTop: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: '#5C5C5C',
        height: 40,
        width: 200,
        padding: 5
    },
    modal: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        backgroundColor: 'rgba(100,100,100, 0.5)',
        padding: 10,
    }
});