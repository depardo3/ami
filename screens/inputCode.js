import React from 'react';
import { Text, View, TextInput, StyleSheet, Button } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Logo from '../assets/loginBG.svg';
import ButtonL from '../assets/buttonLogin.svg';

export default function inputCode({ navigation }) {
    return (
        <View>
            <Logo width='380' height='750' style={Styles.background} />
            <View style={Styles.inputContainer}>
                <TextInput placeholder='INGRESE SU CÓDIGO' style={Styles.codeInput}></TextInput>
            </View>
            <View style={Styles.buttonContainer}>
                <TouchableOpacity onPress={() => navigation.navigate('ListaTratamientos')}>
                    <ButtonL width='200' height='90'>
                    </ButtonL>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const Styles = StyleSheet.create({
    background: {
        position: 'absolute',
        flex: 1,
        marginLeft: -10
    },
    codeInput: {
        textAlign: 'center',
        height: '12%',
        borderRadius: 30,
        marginTop: '100%',
        backgroundColor: 'white',
        width: '85%'
    },
    inputContainer: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    buttonContainer: {
        marginTop: '10%',
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center'
    }
});