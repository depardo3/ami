import React from 'react'
import { View, Text, FlatList, StyleSheet  } from 'react-native';
import Treatmentlabel from '../components/treatment'
const DATATEST = [
    {key:'1', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'2', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'3', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'4', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'5', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'6', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'7', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'},
    {key:'8', fecha:'12/12/2019',tratamiento:'diabetes',doctor:'David Pardo'}      
]
export default function treatmentList({navigation}){
    return(
        <View>
            <FlatList
        data={DATATEST}
        renderItem={({item})=><Treatmentlabel  navigation={navigation} fecha={item.fecha} tratamiento={item.tratamiento} doctor={item.doctor}></Treatmentlabel>}/>
        </View>
    );
}

