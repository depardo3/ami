import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Deco from '../assets/decoTreatList.svg';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Treatmentlabel(props) {
    return (
        <TouchableOpacity style={Styles.container} onPress={() => props.navigation.navigate('Detail')}>
            <Deco></Deco>
            <View style={Styles.descBox}>
                <View style={{flex:1, padding:5}}>
                    <Text>Tratamiento para  {props.tratamiento}</Text>
                    <Text>Dr. {props.doctor}</Text>
                </View>
                <Text style={{padding:5}}>{props.fecha}</Text>
            </View>
        </TouchableOpacity>
    );
}

const Styles = StyleSheet.create({
    descBox: {
        borderWidth: 1,
        width: '70%',
        height: 90,
        borderRadius: 10,
        borderStyle: 'dashed',
        flex: 0,
        flexDirection: 'row'
    },
    container: {
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft:30
    }
});
