import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function LabelTomaActividad(props) {
    return (
        <View style={Styles.container}>
            <View style={Styles.horaBox}><Text style={{ textAlign: 'center' }}>{props.hora} PM</Text></View>
            <View style={Styles.descBox}>
                    <Text>2 Unidades</Text>
                    <Text style={{fontWeight:'bold'}}>{props.main}</Text>
                    <Text>{props.tipo}</Text>
            </View>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 10,
        marginBottom: 10
    },
    horaBox: {
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        width: '20%',
        marginRight: 20,
        padding: 5,
        justifyContent: 'center'
    },
    descBox: {
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        width: '60%',
        padding: 5
    }
})